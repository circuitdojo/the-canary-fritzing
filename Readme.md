# The Canary: A Portable Air Quality Monitor

![Finished product](images/canary.png)

This repository contains the Fritzing for The Canary. [For more information click here.](https://www.hackster.io/jaredwolff/the-canary-a-portable-air-quality-monitor-99a1f4)

[Looking to buy a Particle Squared? Go here.](https://www.jaredwolff.com/store/particle-squared/)
